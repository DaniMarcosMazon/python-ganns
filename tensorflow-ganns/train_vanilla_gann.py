from datasets.MNIST_dataset import load_MNIST_dataset, visualize_data

x_train, x_test, y_train, y_test = load_MNIST_dataset()

print(x_train. shape)
print(x_test. shape)
print(y_train. shape)
print(y_test. shape)

visualize_data(x_train, y_train)